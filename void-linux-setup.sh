#!/usr/bin/bash

set -e

txtblk='\e[0;30m' # Black - Regular
txtgrn='\e[0;32m' # Green

echo -e "${txtgrn}"

function update_system() {
    echo " -> Update and Upgrade Void-Linux"
    sudo xbps-install -Suy xbps > /dev/null 2>&1
    sudo xbps-install -Suy > /dev/null 2>&1
}

function install_kde() {
    echo " -> Install KDE (with some main apps) and Xorg"
    sudo xbps-install -Sy xorg kde5 kde5-baseapps spectacle plasma-disks partitionmanager kcalc > /dev/null 2>&1
    sudo ln -s /etc/sv/dbus /var/service/dbus > /dev/null 2>&1
    sudo ln -s /etc/sv/sddm /var/service/sddm > /dev/null 2>&1
    sudo ln -s /etc/sv/NetworkManager /var/service/NetworkManager > /dev/null 2>&1
}

function install_screen_sharing_deps() {
    echo " -> Install Screen sharing required apps for Wayland"
    sudo xbps-install -Sy xdg-desktop-portal xdg-desktop-portal-gtk xdg-desktop-portal-kde > /dev/null 2>&1
}

function install_riskident_apps() {
    echo " -> Install all RiskIdent required apps"
    sudo xbps-install -Sy clamav ppp openfortivpn seafile-client-qt > /dev/null 2>&1
}

function install_essential_apps() {
    echo " -> Install the essential apps, packages and tools"
    sudo xbps-install -Sy \
         terminator \
         alacritty \
         git \
         zsh \
         wget \
         curl \
         ranger \
         htop \
         netcat \
         p7zip \
         polkit \
         gnupg \
         zip \
         unzip \
         ark \
         audacity \
         lsp \
         pinentry \
         pinentry-gtk \
         pinentry-qt \
         pinentry-tty \
         pinentry-emacs \
         keychain \
         xtool \
         rtkit \
         octoxbps \
         jq > /dev/null 2>&1
    install_emacs
    install_nmap
    install_ohmyzsh
}

function fix_firefox_issue() {
    echo " -> Fix the font issue for firefox"
    sudo ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/ > /dev/null 2>&1
    sudo xbps-reconfigure -f fontconfig > /dev/null 2>&1
}

function install_web_browsers() {
    echo " -> Install Web Browsers"
    sudo xbps-install -Sy firefox torbrowser-launcher chromium > /dev/null 2>&1
    fix_firefox_issue
}

function install_audio_drivers() {
    echo " -> Install and Setup Audio drivers and plugins"
    sudo xbps-install -Sy \
         alsa-utils \
         alsa-firmware \
         alsa-lib \
         alsa-plugins \
         alsa-plugins-ffmpeg \
         pipewire \
         pipewire-devel \
         libpipewire \
         libjack-pipewire \
         alsa-pipewire \
         gstreamer1-pipewire \
         kpipewire \
         kpipewire-devel \
         wireplumber \
         wireplumber-devel > /dev/null 2>&1

    # ThinkPad T14 Laptop
    # sudo xbps-install -Sy \
    #      sof-firmware \
    #      sof-tools > /dev/null 2>&1
    sudo ln -s /etc/sv/alsa /var/service/alsa > /dev/null 2>&1
    sudo sv up alsa > /dev/null 2>&1
    sudo sv enable alsa > /dev/null 2>&1


    sudo ln -s /usr/share/applications/pipewire.desktop /etc/xdg/autostart/pipewire.desktop > /dev/null 2>&1
    sudo ln -s /usr/share/applications/pipewire-pulse.desktop /etc/xdg/autostart/pipewire-pulse.desktop > /dev/null 2>&1
    sudo ln -s /usr/share/applications/wireplumber.desktop /etc/xdg/autostart/wireplumber.desktop > /dev/null 2>&1

    mkdir -p /etc/pipewire/pipewire.conf.d
    sudo ln -s /usr/share/examples/wireplumber/10-wireplumber.conf /etc/pipewire/pipewire.conf.d/
    true "${XDG_CONFIG_HOME:=${HOME}/.config}"
    mkdir -p "${XDG_CONFIG_HOME}/pipewire/pipewire.conf.d"
    sudo ln -s /usr/share/examples/wireplumber/10-wireplumber.conf "${XDG_CONFIG_HOME}/pipewire/pipewire.conf.d/"
}

function install_bluetooth_driver() {
    echo " -> Install and Setup Bluetooth driver"
    sudo xbps-install -Sy bluez libspa-bluetooth > /dev/null 2>&1
    sudo ln -s /etc/sv/bluetoothd /var/service/bluetoothd > /dev/null 2>&1
}

function install_printer_driver() {
    echo " -> Install Printer driver"
    sudo xbps-install -Sy cups cups-filters system-config-printer > /dev/null 2>&1
    # system-config-printer
}

function install_signal() {
    echo " -> Install Signal-Desktop"
    sudo xbps-install -Sy Signal-Desktop > /dev/null 2>&1
}

function install_essential_multimedia_apps() {
    echo " -> Install Multimedia apps"
    sudo xbps-install -Sy libreoffice shotwell vlc guvcview kamoso okular inkscape gimp ffmpeg > /dev/null 2>&1
}

function install_flatpak() {
    echo " -> Install Flatpak"
    sudo xbps-install -Sy flatpak > /dev/null 2>&1
    sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo > /dev/null 2>&1
}

function install_podman() {
    echo " -> Install Podman"
    sudo xbps-install -Sy podman > /dev/null 2>&1
}

function install_docker() {
    echo " -> Install and Setup Docker and Containerd"
    sudo xbps-install -Sy docker containerd > /dev/null 2>&1
    sudo ln -s /etc/sv/docker /var/service/docker > /dev/null 2>&1
    sudo ln -s /etc/sv/containerd /var/service/containerd > /dev/null 2>&1
    sudo groupadd docker > /dev/null 2>&1
    sudo usermod -aG docker $USER > /dev/null 2>&1
}

function install_npm() {
    echo " -> Install NVM/NPM"

    NODE_LTS_VERSION=18

    wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash > /dev/null 2>&1
    export NVM_DIR="$HOME/.nvm" > /dev/null 2>&1
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  > /dev/null 2>&1 # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" > /dev/null 2>&1 # This loads nvm bash_completion

    nvm install ${NODE_LTS_VERSION} > /dev/null 2>&1
    nvm use ${NODE_LTS_VERSION} > /dev/null 2>&1
}

function install_pyright() {
    echo " -> Install Pyright"
    pip install pyright --user > /dev/null 2>&1
}

function install_essential_dev_tools() {
    echo " -> Install Dev tools"
    sudo xbps-install -Sy cmake libtool autoconf texinfo \
         python3-devel python3-pip \
         ruby-devel \
         openjdk8 openjdk11 openjdk17 > /dev/null 2>&1
    install_npm
    install_pyright
    install_rust
}

function install_rust() {
    echo " --> Install Rust Programming Language"

    # Pick the correct `rustup-init` for your platform for here
    # https://forge.rust-lang.org/infra/other-installation-methods.html
    curl -o rustup-init https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init -sSf > /dev/null 2>&1
    chmod +x rustup-init > /dev/null 2>&1
    ./rustup-init -y > /dev/null 2>&1

    source ~/.cargo/env

    # Install rust-analyzer for lsp
    echo " ---> Install rust-analyzer for lsp"
    rustup component add rust-analyzer > /dev/null 2>&1

}
function install_virtual_manager() {
    echo " -> Install virt-manage, qemu and libvirt etc. and create daemon services"
    sudo xbps-install -Sy libvirt qemu virt-manager bridge-utils iptables > /dev/null 2>&1
    sudo ln -s /etc/sv/libvirtd /var/service/libvirtd > /dev/null 2>&1
    sudo ln -s /etc/sv/virtlockd /var/service/virtlockd > /dev/null 2>&1
    sudo ln -s /etc/sv/virtlogd /var/service/virtlogd > /dev/null 2>1&
    sudo ln -s /etc/sv/virtqemud /var/service/virtqemud > /dev/null 2>&1
    sudo ln -s /etc/sv/libvirt-generic /var/service/libvirt > /dev/null 2>&1
    sudo sv up libvirt > /dev/null 2>&1
    sudo sv enable libvirt > /dev/null 2>&1
}

function install_virtualbox() {
    echo " -> Install and Setup VirtualBox with Guest Additions"
    sudo xbps-install -Sy virtualbox-ose virtualbox-ose-dkms virtualbox-ose-guest-dkms linux-headers linux6.2-headers > /dev/null 2>&1
    sudo groupadd vboxusers > /dev/null 2>&1
    sudo usermod -a -G vboxusers $USER > /dev/null 2>&1
}

function install_fonts() {
    echo " -> Install all sort of fonts"
    sudo xbps-install -Sy \
         font-awesome \
         font-hack-ttf \
         terminus-font \
         wqy-microhei \
         txtw-0.4_2 \
         woff2-1.0.2_1 \
         wqy-microhei-0.2.0.beta_3 \
         xfd-1.1.4_1 \
         ttf-ubuntu-font-family-0.83_3 \
         noto-fonts-ttf-extra \
         fcitx \
         fonts-nanum-ttf \
         fonts-nanum-ttf-extra \
         amiri-font \
         noto-fonts-cjk \
         noto-fonts-emoji \
         font-material-design-icons-ttf \
         fonts-roboto-ttf \
         font-iosevka > /dev/null 2>&1

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/install_manual.sh)" > /dev/null 2>&1
}

function install_spelling_checks() {
    echo " -> Install spelling checks and language support (Arabic, English and German)"
    sudo xbps-install -Sy ibus-hangul tesseract-ocr-ara tesseract-ocr-deu \
         words-de ding aspell-de \
         hunspell-en_GB hunspell-en_US aspell-en aspell > /dev/null 2>&1
}

function install_devops_tools() {
    echo " -> Install DevOps tools"
    sudo xbps-install -Sy \
         kubectl \
         kubernetes-helm \
         vagrant \
         terraform \
         consul \
         vault \
         nomad \
         kubernetes-kind \
         terragrunt \
         ansible \
         git-crypt \
         act \
         aws-cli \
         github-cli \
         grip > /dev/null 2>&1

    # Install ArgoCD CLI
    curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64 > /dev/null 2>&1
    sudo install -m 555 argocd-linux-amd64 /usr/local/bin/argocd > /dev/null 2>&1
    rm argocd-linux-amd64 > /dev/null 2>&1
}

VOID_PACKAGES_DIR="void-packages"

function clone_void_packages() {
    echo " -> Clone void-packages github repo"
    if [ ! -d "$VOID_PACKAGES_DIR" ]; then
        git clone --quiet https://github.com/void-linux/void-packages.git
    else
        git -C $VOID_PACKAGES_DIR pull --quiet
    fi
}

function install_emacs() {
    echo " ---> Build and Install Emacs from source code"
    ./$VOID_PACKAGES_DIR/xbps-src binary-bootstrap > /dev/null 2>&1
    ./$VOID_PACKAGES_DIR/xbps-src pkg emacs-x11 > /dev/null 2>&1
    sudo xbps-install --repository $VOID_PACKAGES_DIR/hostdir/binpkgs emacs-x11 > /dev/null 2>&1
}

function install_slack() {
    echo " -> Build and Install Slack-Desktop from source code"
    ./$VOID_PACKAGES_DIR/xbps-src binary-bootstrap > /dev/null 2>&1
    ./$VOID_PACKAGES_DIR/xbps-src pkg slack-desktop > /dev/null 2>&1
    sudo xbps-install --repository $VOID_PACKAGES_DIR/hostdir/binpkgs/nonfree slack-desktop > /dev/null 2>&1
}

function install_nmap() {
    echo " ---> Build and Install Nmap from source code"
    ./$VOID_PACKAGES_DIR/xbps-src binary-bootstrap > /dev/null 2>&1
    ./$VOID_PACKAGES_DIR/xbps-src pkg nmap > /dev/null 2>&1
    sudo xbps-install --repository $VOID_PACKAGES_DIR/hostdir/binpkgs/nonfree nmap > /dev/null 2>&1
}

function generate_ssh_keypairs() {
    echo " -> Generate and Setup GitHub and GitLab SSH"

    ssh-keygen -q -t ed25519 -N '' -f ~/.ssh/github <<<y > /dev/null 2>&1
    ssh-keygen -q -t ed25519 -N '' -f ~/.ssh/gitlab <<<y > /dev/null 2>&1

    cat <<EOF > ~/.ssh/config
# GitHub.com
Host github.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/github

# GitLab.com
Host gitlab.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/gitlab
EOF

    echo "You should add the public keys to it's respective website..."
}


function install_ohmyzsh() {
    echo " ---> Install OhMyZsh"
    rm -rf ~/.oh-my-zsh
    git clone --quiet https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh > /dev/null 2>&1
    # cp ~/.zshrc ~/.zshrc.orig > /dev/null 2>&1
    cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc > /dev/null 2>&1
    sudo chsh -s $(which zsh) > /dev/null 2>&1
}

function setup_void_system() {
    update_system

    clone_void_packages

    install_kde
    install_essential_apps
    install_web_browsers
    install_essential_multimedia_apps
    install_screen_sharing_deps
    install_essential_dev_tools
    install_devops_tools

    install_fonts
    install_spelling_checks

    install_riskident_apps

    install_audio_drivers
    install_bluetooth_driver
    install_printer_driver

    # SSH Key pairs
    generate_ssh_keypairs

    # Desktop Apps
    install_signal
    install_slack

    # Containerization
    install_flatpak
    install_podman
    install_docker

    # Virtualizations
    install_virtual_manager
    install_virtualbox
}

function cleanup() {
    echo " -> Clean Up"
    if [ -d "$VOID_PACKAGES_DIR" ]; then
        rm -rf $VOID_PACKAGES_DIR
    fi
    rm rustup-init
}

setup_void_system
# cleanup
